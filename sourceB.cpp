#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

int callback(void* notUsed, int argc, char** argv, char** azCol);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
void clearTable();
void checkAnswer(bool ans);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);
unordered_map<string, vector<string>> results;

int main()
{
	char *zErrMsg = 0;
	bool ans;

	//Connect to database
	sqlite3* db;
	int rc = sqlite3_open("carsDealer.db", &db);

	//Check if the database opened successfully
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		system("PAUSE");
		return(1);
	}

	cout << "A:" << endl;
	//Check all three purchases (first - unsuccessful, second - successful, third - successful)
	ans = carPurchase(10, 2, db, zErrMsg);
	checkAnswer(ans);
	ans = carPurchase(9, 20, db, zErrMsg);
	checkAnswer(ans);
	ans = carPurchase(12, 3, db, zErrMsg);
	checkAnswer(ans);

	cout << "B:" << endl;
	//Make sure the transfering has no problems
	ans = balanceTransfer(1, 3, 150, db, zErrMsg);
	checkAnswer(ans);

	sqlite3_close(db);

	system("PAUSE");
	return 0;
}

/*
	Input: id of the person who transfer money, 
	       id of the person who getting the money,
		   the amount of money that is being transfered,
		   a pointer to the database,
		   an error message.
	Output: whether the transfer has been made successfully or not.
	The function transfers money from one person to another
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	bool ans = false;

	//Makes sure there are no problems while doing the transfer
	string str = "begin transaction";
	int rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	//Get the balance of the person that transfers money
	str = "select balance from accounts WHERE id=" + to_string(from);
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	auto it = results.begin();
	string temp = it->second.at(0);
	int fromBalance = stoi(temp);

	clearTable();

	//Get the balance of the person that gets the money
	str = "select balance from accounts WHERE id=" + to_string(to);
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	it = results.begin();
	temp = it->second.at(0);
	int toBalance = stoi(temp);

	//Check if the first person has enough money to transfer
	if (fromBalance >= amount)
	{
		fromBalance -= amount; //Reduce the amount money from that person
		toBalance += amount; //Increse the amount money of the second person
		
		//Update their balances
		str = "update accounts set balance=" + to_string(fromBalance) + " where id=" + to_string(from);
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		str = "update accounts set balance=" + to_string(toBalance) + " where id=" + to_string(to);
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		
		ans = true;
	}

	//End the transaction
	str = "commit";
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	return ans;
}

/*
	Input: true / false
	Output: none
	The function prints if the action was successfull or not based on the parameter (true - successful, false - not)
*/
void checkAnswer(bool ans)
{
	if (ans)
	{
		cout << "successful." << endl;
	}
	else
	{
		cout << "not successful." << endl;
	}
}

/*
	Input: none
	Output: none
	The function clears the map
*/
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it) //Go through all the results in the map
	{
		it->second.clear(); //Clear each one of them
	}
	results.clear();
}

/*
	Input: The buyers id,
	       The car id,
		   a pointer to the database,
		   an error message.
	Output: whether the buyer bought the car or not
	The function checks if the buyer has enough money to buy the car and if so it updates the balance of the buyer
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool ans = false;

	//Get the balance of the buyer
	string str = "select * from accounts WHERE id=" + to_string(buyerid);
	int rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	auto it = results.end();
	it--;
	string temp = it->second.at(0);
	int balance = stoi(temp);

	//Get if the car is available to buy or not
	str = "select * from cars WHERE id=" + to_string(carid);
	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	it = results.begin();
	temp = it->second.at(0);
	int available = stoi(temp);

	//Get the price of the car
	while (it->first != "price")
	{
		it++;
	}
	temp = it->second.at(0);
	int price = stoi(temp);

	//Check if the buyer can buy the car (if the car is available and if the buyer has enough money)
	if (available == 1 && balance >= price)
	{
		//Update the car so that it won't be available anymore
		str = "update cars set available=0 where id=" + to_string(carid);
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		balance -= price; //Reduce the balance of the buyer

		//Update the balance of the buyer
		str = "update accounts set balance=" + to_string(balance) + " where id=" + to_string(buyerid);
		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

		ans = true;
	}

	clearTable();

	return ans;
}

/*
	The function inserts the results of the sql order to the global map and saves them
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

