#include "sqlite3.h"
#include <iostream>
#include <string>

using namespace std;

int callback(void* notUsed, int argc, char** argv, char** azCol);

int main()
{
	char *zErrMsg = 0;

	//Connect to database
	sqlite3* db;
	int rc = sqlite3_open("FirstPart.db", &db);

	//Check if the opening of the database was successful
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		system("PAUSE");
		return(1);
	}

	cout << "Before updating:" << endl;

	//Create new table and insert data
	rc = sqlite3_exec(db, "create table people(id integer primary key, name string);", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Noa');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Keren');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "insert into people(name) values('Roy');", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "select * from people;", callback, 0, &zErrMsg);

	cout << "\nAfter updating:" << endl;

	//Update one name in the table
	rc = sqlite3_exec(db, "update people set name='Efrat' where id=last_insert_rowid();", NULL, 0, &zErrMsg);
	rc = sqlite3_exec(db, "select * from people;", callback, 0, &zErrMsg);

	sqlite3_close(db);

	system("PAUSE");
	return 0;
}

/*
The function prints the info of the table
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		cout << azCol[i] << " = " << argv[i] << endl;
	}

	return 0;
}